package com.glitchcube.localization;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 12.06.2016
 * Time: 00.03
 */
@Path("properties")
public class Properties {

    @GET
    public String properties() {
        return System.getenv().toString();
    }
}
