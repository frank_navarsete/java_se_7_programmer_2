package com.glitchcube.localization;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Frank Navarsete on 28.06.2016.
 */
public class TalkativeResourceBundleControl extends ResourceBundle.Control{

    @Override
    public List<Locale> getCandidateLocales(String baseName, Locale locale) {
        List<Locale> candidateLocales = super.getCandidateLocales(baseName, locale);
        candidateLocales.forEach(localeCandidates -> System.out.println("TalkativeResourceBundleControl " + localeCandidates.getDisplayName()));
        return candidateLocales;
    }
}
