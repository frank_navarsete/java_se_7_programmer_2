package com.glitchcube.localization.listresourcebundle;

import java.util.ListResourceBundle;

/**
 * @author: glitchcube.com
 */
public class ResBundle_nb_NO extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents = {
        {"MovieName", "Bølgen"},
        {"GrossRevenue", 2785172L},
        {"Year", 2015}
    };
}
