package com.glitchcube.localization.listresourcebundle;

import java.util.ListResourceBundle;

/**
 * @author: glitchcube.com
 */
public class ResBundle extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents = {
        {"MovieName", "Avatar"},
        {"GrossRevenue", 2782275172L},
        {"Year", 2009}
    };
}
