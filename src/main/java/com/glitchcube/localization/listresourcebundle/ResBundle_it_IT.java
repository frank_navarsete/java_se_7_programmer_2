package com.glitchcube.localization.listresourcebundle;

import java.util.ListResourceBundle;

/**
 * @author: glitchcube.com
 */
public class ResBundle_it_IT extends ListResourceBundle {
    @Override
    protected Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents = {
            { "MovieName", "Che Bella Giornata" },
            { "GrossRevenue", (Long) 43000000L }, // in euros
            { "Year", (Integer)2011 }
    };
}
