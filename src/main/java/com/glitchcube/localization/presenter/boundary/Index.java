package com.glitchcube.localization.presenter.boundary;

import javax.enterprise.inject.Model;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author: glitchcube.com
 */

@Model
public class Index {


    public String getMessage() {
        ResourceBundle resourceBundle = ResourceBundle.getBundle("message", Locale.US);

        return resourceBundle.getString("index.hello");
    }
}
