package com.glitchcube.localization;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 11.06.2016
 * Time: 23.53
 */
@Path("messages")
@ApplicationScoped
public class Messages {

    private String message;

    @GET
    public String retrieveMessages() {
        return message;
    }

    @POST
    public void putMessage(String message) {
        this.message = message;
    }
}
