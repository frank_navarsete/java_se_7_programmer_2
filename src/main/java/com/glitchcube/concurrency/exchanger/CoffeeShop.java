package com.glitchcube.concurrency.exchanger;

import java.util.concurrent.Exchanger;

/**
 * @author: glitchcube.com
 */
class CoffeeShop extends Thread {

    private final Exchanger<String> sillyTalk;

    CoffeeShop(Exchanger<String> sillyTalk) {
        this.sillyTalk = sillyTalk;
        start();
    }

    @Override
    public void run() {
        String reply;
        try {
            // exchange the first messages
            reply = sillyTalk.exchange("Who's there?");
            // print what Duke said
            System.out.println("Duke: " + reply);
            // exchange second message
            reply = sillyTalk.exchange("Duke who?");
            // print what Duke said
            System.out.println("Duke: " + reply);
            // there is no message to send, but to get a message from Duke thread,
            // both ends should send a message; so send a "dummy" string
            reply = sillyTalk.exchange("");
            System.out.println("Duke: " + reply);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
