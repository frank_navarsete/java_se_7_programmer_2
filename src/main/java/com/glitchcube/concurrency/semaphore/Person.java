package com.glitchcube.concurrency.semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author : glitchcube.com
 */
class Person extends Thread {

    private final Semaphore machine;

    Person(Semaphore machine, String name) {
        this.machine = machine;
        setName(name);
        start();
    }

    @Override
    public void run() {
        try {
            System.out.println(getName() + " waiting to get access to an ATM machine");
            //Check if a machine is available
            machine.acquire();
            System.out.println(getName() + " is accessing an ATM machine");
            Thread.sleep(1000); // simulate the time required for withdrawing amount.
            System.out.println(getName() + " is done using the ATM machine");
            machine.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
