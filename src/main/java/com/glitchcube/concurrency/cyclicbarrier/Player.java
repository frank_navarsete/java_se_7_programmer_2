package com.glitchcube.concurrency.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author: glitchcube.com
 */
class Player extends Thread {

    private final CyclicBarrier barrier;

    Player(CyclicBarrier barrier, String name) {
        setName(name);
        this.barrier = barrier;
        start();
    }

    @Override
    public void run() {
        System.out.println("Player " + getName() + " is ready ");

        try {
            barrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
