package com.glitchcube.concurrency.cyclicbarrier;

/**
 * @author: glitchcube.com
 */
class MixedDoubleTennisGame extends Thread {

    @Override
    public void run() {
        System.out.println("Let the game begin!!!");
    }
}
