package com.glitchcube.concurrency.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * @author: glitchcube.com
 */
class Runner extends Thread {

    private final CountDownLatch timer;

    Runner(CountDownLatch timer, String name) {
        setName(name);
        this.timer = timer;
        System.out.println(getName() + " ready and waiting for the count down to start");
        start();
    }

    @Override
    public void run() {
        try {
            //Wait for the timer to count down to zero
            timer.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(getName() + " started running");
    }
}
