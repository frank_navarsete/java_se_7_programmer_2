package com.glitchcube.concurrency.phaser;

import java.util.concurrent.Phaser;

/**
 * @author: glitchcube.com
 *
 */
class Worker extends Thread {

    private final Phaser deliveryOrder;

    Worker(Phaser deliveryOrder, String name) {
        setName(name);
        this.deliveryOrder = deliveryOrder;
        deliveryOrder.register();
        start();
    }

    // The work could be a Cook, Helper, or Attendant. Though the three work independently, the
    // should all synchronize their work together to do their part and complete preparing a food item
    @Override
    public void run() {
        for (int i = 1; i <= 3; i++) {
            System.out.println("\t " + getName() + " doing his work for order no . " + i);
            if(i == 3) {
                //work complete for this delivery order, so deregister
                deliveryOrder.arriveAndDeregister();
            } else {
                deliveryOrder.arriveAndAwaitAdvance();
            }
            try {
                Thread.sleep(3000); // simulate time for preparing the food item.
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
