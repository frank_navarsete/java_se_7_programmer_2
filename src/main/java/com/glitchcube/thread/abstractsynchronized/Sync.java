package com.glitchcube.thread.abstractsynchronized;

/**
 * @author: glitchcube.com
 */
public abstract class Sync {

    //Not allows because it don't have a body
    //public synchronized abstract void foo();

    public synchronized void foo() {}
}
