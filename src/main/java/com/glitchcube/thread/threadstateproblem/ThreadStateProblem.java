package com.glitchcube.thread.threadstateproblem;

/**
 * @author: glitchcube.com
 */
public class ThreadStateProblem extends Thread {

    @Override
    public void run() {
        try {
            wait(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
