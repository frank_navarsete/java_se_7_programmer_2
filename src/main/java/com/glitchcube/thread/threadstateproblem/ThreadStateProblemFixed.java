package com.glitchcube.thread.threadstateproblem;

/**
 * @author: glitchcube.com
 */
public class ThreadStateProblemFixed extends Thread {

    @Override
    //add synchronized to acquire a lock on the class to be able to set the class in wait state
    synchronized public void run() {
        try {
            wait(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
