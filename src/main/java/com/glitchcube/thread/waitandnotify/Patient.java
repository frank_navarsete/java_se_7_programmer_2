package com.glitchcube.thread.waitandnotify;

/**
 * @author: glitchcube.com
 */
class Patient extends Thread {

    private final Doctor doctor;
    private String patientName;

    Patient(Doctor doctor, String patientName) {
        this.doctor = doctor;
        this.patientName = patientName;
    }

    @Override
    public void run() {
        synchronized (doctor) {
            while (!doctor.isAvailable()) {
                try {
                    System.out.println("PATIENT: Doctor not available for " + patientName + ". Start waiting...");
                    doctor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("PATIENT: Doctor available for patient " + patientName );
            doctor.checkPatient(patientName);

            doctor.setAvailable(true);
            doctor.notifyAll();
        }
    }

}
