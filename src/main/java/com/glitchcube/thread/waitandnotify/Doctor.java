package com.glitchcube.thread.waitandnotify;

/**
 * @author: glitchcube.com
 */
class Doctor {
    private boolean available = true;

    boolean isAvailable() {
        return available;
    }

    void setAvailable(boolean available) {
        this.available = available;
    }

    void checkPatient(String patientName) {

        System.out.println("DOCTOR: start checking patient " + patientName);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("DOCTOR: done inspecting " + patientName);
    }
}
