package com.glitchcube.thread.dicegame;

import java.util.Random;

/**
 * the Dice class abstracts how the dice rolls and who plays it
 * @author: glitchcube.com
 */
class Dice {

    private static String turn = null;
    private static Random random = new Random();


    private Dice() {
    }

    synchronized static String getTurn() {
        return Dice.turn;
    }

    synchronized static void setTurn(String name) {
        Dice.turn = name;
    }

    static void whoStarts(String name) {
        Dice.turn = name;
    }


    static int roll() {
        return random.nextInt(6)+1;
    }
}
