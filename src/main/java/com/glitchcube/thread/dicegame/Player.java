package com.glitchcube.thread.dicegame;

/**
 * @author: glitchcube.com
 */
class Player extends Thread {

    private String currentPlayer = null;
    private String otherPlayer = null;

    Player(String thisPlayer) {
        this.currentPlayer = thisPlayer;

        otherPlayer = thisPlayer.equals(Gamers.JOE) ? Gamers.JANE : Gamers.JOE;
    }

    @Override
    public void run() {
        for (int i = 0; i < 6; i++) {
            synchronized (Dice.class) {
                while (!Dice.getTurn().equals(currentPlayer)) {
                    try {
                        Dice.class.wait(1000);
                        System.out.println(currentPlayer +
                                " was waiting for " + otherPlayer);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }


                // its currentPlayer's turn now; throw the dice
                System.out.println(Dice.getTurn() + " throws" + Dice.roll());
                Dice.setTurn(otherPlayer);
                Dice.class.notifyAll();
            }
        }
    }
}
