package com.glitchcube.localization;

import org.junit.Test;

import java.util.Enumeration;
import java.util.ResourceBundle;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: Frank
 * Date: 18.06.2016
 * Time: 19.28
 */
public class readResourceBundleTest {

    @Test
    public void listKeysInResourceBoundle() {
        ResourceBundle messages = ResourceBundle.getBundle("message");
        Enumeration<String> keys = messages.getKeys();
        while(keys.hasMoreElements()) {
            System.out.println(keys.nextElement());
        }
    }

}
