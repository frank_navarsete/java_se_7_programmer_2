package com.glitchcube.localization;

import org.junit.Test;

import java.util.Locale;

/**
 * @author: glitchcube.com
 */
public class LocaleListTest {

    @Test
    public void listAllLocaleAvailable() {
        System.out.println("THe default locale is: " + Locale.getDefault());

        Locale[] availableLocales = Locale.getAvailableLocales();
        System.out.printf("No. of other available locales: %d, and they are: %n", availableLocales.length);
        for (Locale locale : availableLocales) {
            System.out.printf("Locale code: %s and it stands for %s %n", locale,locale.getDisplayName());
        }
    }

    @Test
    public void listAllEnglishLocale() {
        for (Locale locale : Locale.getAvailableLocales()) {
            if(locale.getLanguage().equals("en")) {
                System.out.printf("Locale code %s and it stands for %s %n", locale, locale.getDisplayName());
            }
        }
    }
}
