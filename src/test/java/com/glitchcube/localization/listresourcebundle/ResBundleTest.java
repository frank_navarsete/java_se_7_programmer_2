package com.glitchcube.localization.listresourcebundle;

import org.junit.Test;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author: glitchcube.com
 */
public class ResBundleTest {

    @Test
    public void loadDefaultResBundleListResource() {
        Locale italy = Locale.ITALY;
        //Important: Remember to specify the path to where ther resources are located.
        ResourceBundle resBundle = ResourceBundle.getBundle("com.glitchcube.listresourcebundle.ResBundle");
        ResourceBundle italyResBundle = ResourceBundle.getBundle("com.glitchcube.listresourcebundle.ResBundle", italy);
        String movieName = resBundle.getString("MovieName");
        Long grossRevenue = (Long) resBundle.getObject("GrossRevenue");
        Integer year = (Integer) resBundle.getObject("Year");

        System.out.printf("Movie %s (%d) grossed %d", movieName, grossRevenue, year);
    }
}