package com.glitchcube.localization;

import org.junit.Test;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/**
 * @author glitchcube.com
 */
public class NumberFormatTest {

    @Test
    public void germanNumberFormat() {
        long tenMillion = 10_000_000L;

        NumberFormat germanFormat = NumberFormat.getInstance(Locale.GERMAN);
        String localizedTenMillion = germanFormat.format(tenMillion);

        System.out.println("Ten million in German local is: " + localizedTenMillion);

        try {
            Number parsedAmount = germanFormat.parse(localizedTenMillion);
            if(tenMillion == parsedAmount.longValue()) {
                System.out.println("Successfully parsed the number for the locale");
            }
        } catch (ParseException e) {
            System.err.println("Error: Cannot parse the number for the locale");
        }
    }

    @Test
    public void fetchCurrency() {
        Locale locale = Locale.getDefault();
        Currency currencyInstance = Currency.getInstance(locale);
        System.out.println("The currecny code for the locale " + locale + " is: " + currencyInstance.getCurrencyCode()
                + " \n The currency symbol is " + currencyInstance.getSymbol()
                + " \n The currency name is " + currencyInstance.getDisplayName());
    }

    @Test
    public void dateFormat() {
        Date today = new Date();
        Locale[] locales = {Locale.CANADA, Locale.FRANCE, Locale.GERMANY, Locale.ITALY, Locale.getDefault()};

        for (Locale locale : locales) {
            // DateFormat.FULL refers to the full details of the date
            DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, locale);
            System.out.println("Date in locale " + locale + " is: " + dateFormat.format(today));
        }
    }
}
