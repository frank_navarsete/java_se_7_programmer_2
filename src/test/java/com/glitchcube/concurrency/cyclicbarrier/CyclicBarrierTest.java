package com.glitchcube.concurrency.cyclicbarrier;

import org.junit.Test;

import java.util.concurrent.CyclicBarrier;

/**
 * @author: glitchcube.com
 */
public class CyclicBarrierTest {

    @Test
    public void cyclicBarrier() throws InterruptedException {
        // a mixed-double tennis game requires four players; so wait for four players
        // (i.e., four threads) to join to start the game
        System.out.println("Reserving tennis court \n As soon as four players arrive, game will start");

        CyclicBarrier barrier = new CyclicBarrier(4, new MixedDoubleTennisGame());
        Player player = new Player(barrier, "G.I Joe");
        Player player2 = new Player(barrier, "Dora");
        Player player3 = new Player(barrier, "Tintin");
        Player player4 = new Player(barrier, "Barbie");

        player.join();
        player2.join();
        player3.join();
        player4.join();

    }
}
