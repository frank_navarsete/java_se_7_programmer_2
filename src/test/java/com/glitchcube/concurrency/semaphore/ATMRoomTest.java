package com.glitchcube.concurrency.semaphore;

import org.junit.Test;

import java.util.concurrent.Semaphore;

/**
 * @author: glitchcube.com
 */
public class ATMRoomTest {


    // This class simulates a situation where an ATM room has only two ATM machines
    // and five people are waiting to access the machine. Since only one person can access
    // an ATM machine at a given time, others wait for their turn
    @Test
    public void operateATMRoom() throws InterruptedException {
        Semaphore machine = new Semaphore(2);
        Person mickey = new Person(machine, "Mickey");
        Person frank = new Person(machine, "Frank");
        Person iselin = new Person(machine, "Iselin");
        Person glitch = new Person(machine, "Glitch");
        Person tom = new Person(machine, "Tom");


        //Join to ensure the threads get done.
        mickey.join();
        frank.join();
        iselin.join();
        glitch.join();
        tom.join();

    }
}