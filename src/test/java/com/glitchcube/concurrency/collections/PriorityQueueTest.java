package com.glitchcube.concurrency.collections;

import org.junit.Test;

import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * @author: glitchcube.com
 */
public class PriorityQueueTest {

    @Test
    public void priorityQueueWithoutThreadingInMind() {
        final PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        new Thread() {
            @Override
            public void run() {
                Integer integerRemoved = priorityQueue.remove();
                System.out.println("The following integer was removed " + integerRemoved);

            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                priorityQueue.add(10);
                System.out.println("Adding to the priority queue");
            }
        }.start();


    }

    @Test
    public void priorityQueueWithThreadingSupport() throws InterruptedException {
        final PriorityBlockingQueue<Integer> priorityBlockingQueue = new PriorityBlockingQueue<>();

        Thread threadRemoving = new Thread() {
            @Override
            public void run() {
                try {
                    System.out.println("Ready to take from the queue");
                    Integer takenInteger = priorityBlockingQueue.take();
                    System.out.println("Took following integer from the queue " + takenInteger);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        Thread threadAdding = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                priorityBlockingQueue.add(10);
                System.out.println("Adding integer to the queue");
            }
        };


        threadRemoving.start();
        threadAdding.start();

        threadAdding.join();
        threadRemoving.join();
    }
}
