package com.glitchcube.concurrency.countdownlatch;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * @author: glitchcube.com
 */
public class RunningRaceStarterTest {

    // this class simulates the start of a running race by counting down from 5. It holds
    // three runner threads to be ready to start in the start line of the race and once the count down
    // reaches zero, all the three runners start running...
    @Test
    public void runningRaceStarter() throws InterruptedException {
        CountDownLatch counter = new CountDownLatch(5);

        new Runner(counter, "Carl");
        new Runner(counter, "Joe");
        new Runner(counter, "Jack");

        System.out.println("Start countdown");

        long countValue = counter.getCount();
        while(countValue > 0) {
            Thread.sleep(1000);
            System.out.println(countValue);
            if(countValue == 1) {
                // once counter.countDown(); in the next statement is called,
                // Count down will reach zero; so shout "Start"
                System.out.println("Start!");
            }
            counter.countDown();
            countValue = counter.getCount();

        }
    }
}