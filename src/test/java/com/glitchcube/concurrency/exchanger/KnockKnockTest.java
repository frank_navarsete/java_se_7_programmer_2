package com.glitchcube.concurrency.exchanger;

import org.junit.Test;

import java.util.concurrent.Exchanger;

/**
 * @author: glitchcube.com
 */
public class KnockKnockTest {


    //Wired bug. The CoffeeShop prints out the two time in a row, but should go back and forward between
    //Duke thread and CoffeeShop thread.
    @Test
    public void knockKnockTest() throws InterruptedException {
        Exchanger<String> sillyTalk = new Exchanger<String>();

        DukeThread dukeThread = new DukeThread(sillyTalk);
        CoffeeShop coffeeShop = new CoffeeShop(sillyTalk);

        //wait for the threads to be done talking to each other.
        dukeThread.join();
        coffeeShop.join();
    }
}
