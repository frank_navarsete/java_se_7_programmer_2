package com.glitchcube.thread.threadstateproblem;

import org.junit.Test;

/**
 * @author: glitchcube.com
 */
public class ThreadStateProblemTest {

    @Test
    public void createIllegalMonitorStateException() {
        new ThreadStateProblem().start();
    }

    @Test
    public void illegalMontiorStateFixed() {
        new ThreadStateProblemFixed().start();
    }
}