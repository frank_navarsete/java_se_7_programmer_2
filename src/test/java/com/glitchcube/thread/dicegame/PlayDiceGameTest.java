package com.glitchcube.thread.dicegame;

import org.junit.Test;

/**
 * @author: glitchcube.com
 */
public class PlayDiceGameTest {

    @Test
    public void playGame() {
        Player jane = new Player(Gamers.JANE);
        Player joe = new Player(Gamers.JOE);

        Dice.whoStarts(Gamers.JOE);
        jane.start();
        joe.start();
    }
}
