package com.glitchcube.thread.waitandnotify;

import org.junit.Test;

/**
 * @author: glitchcube.com
 */
public class DockerAndPatient {

    @Test
    public void takeCareOfPatient() throws InterruptedException {
        Doctor doctor = new Doctor();

        Patient patient1 = new Patient(doctor,"patient1");
        Patient patient2 = new Patient(doctor,"patient2");

        patient1.start();
        patient2.start();

        patient1.join();
        patient2.join();
    }
}
