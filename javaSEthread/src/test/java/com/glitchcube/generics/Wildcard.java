package com.glitchcube.generics;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author glitchcube.com
 */
public class Wildcard {

    @Test
    public void addingObjectToWildcardList() {
        List<? extends Number> wildCardList = new ArrayList<Integer>();
//        wildCardList.add(new Integer(10)); //Not allowed to edit object with wildcard
        System.out.println(wildCardList);
    }

    @Test
    public void boundedWildcard() {
        List<?> wildcardList = new ArrayList<Integer>();
        wildcardList = new ArrayList<String>();

        List<? extends Number> wildCardLockToNumber = new ArrayList<Integer>();
//        wildCardLockToNumber = new ArrayList<String>(); //Not allowed change to a type that is not extending from Number
        wildCardLockToNumber = new ArrayList<Double>();
    }
}
