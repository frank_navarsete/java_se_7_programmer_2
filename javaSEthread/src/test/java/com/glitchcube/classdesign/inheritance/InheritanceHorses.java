package com.glitchcube.classdesign.inheritance;

import org.junit.Test;

/**
 * @author glitchcube.com
 */
public class InheritanceHorses {

    @Test
    public void rideHorse() {
        Rideable r1 = new Icelandic();
        Rideable r2 = new Horse();
        Horse h1 = new Icelandic();

        System.out.println(r1.ride() + r2.ride() + h1.ride());
    }

    //Interface method are default public.
    interface Rideable {
        String ride();
    }

    //Classes are default protected.
    class Horse implements Rideable {

        public String ride() {
            return "cantering";
        }
    }

    class Icelandic extends Horse {

        public String ride() {
            return "tolting";
        }
    }




}
