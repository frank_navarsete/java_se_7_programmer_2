package com.glitchcube.classdesign.inheritance;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author glitchcube.com
 */
public class FieldShadowing {

    @Test
    public void variableToSelect() {
        int fromA = 5;
        int fromB = 7;
        A classB = new B();
        assertThat(classB.a, is(fromA));

        B clazz = new B();
        assertThat(clazz.a, is(fromB));
    }

    class A {
        int a = 5;
    }

    class B extends A {
        int a = 7;
    }
}
