package com.glitchcube.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author glitchcube.com
 */
public class DBConnectionEstablisherLocalhost {

    static final String BASE_URL = "jdbc:mysql://192.168.99.100:3306/";
    static final String DATABASE = "addressBook";
    static final String COMPLETE_URL = BASE_URL + DATABASE;
    static final String USERNAME = "root";
    static final String PASSWORD = "password";

    public static Connection getDbConnection() throws SQLException {

        return DriverManager.getConnection(COMPLETE_URL, USERNAME, PASSWORD);
    }
}
