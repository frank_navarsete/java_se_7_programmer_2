package com.glitchcube.database;

import org.junit.Test;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.SQLException;

/**
 * @author glitchcube.com
 */
public class DBRowSetProviderFactoryTest {

    @Test
    public void queryRowWithRowSetPRoviderFactory() {

        try {
            RowSetFactory rowSetFactory = RowSetProvider.newFactory();
            JdbcRowSet jdbcRowSet = rowSetFactory.createJdbcRowSet();
            jdbcRowSet.setUrl(DBConnectionEstablisherLocalhost.COMPLETE_URL);
            jdbcRowSet.setUsername(DBConnectionEstablisherLocalhost.USERNAME);
            jdbcRowSet.setPassword(DBConnectionEstablisherLocalhost.PASSWORD);
            jdbcRowSet.setCommand("SELECT * FROM contact");
            jdbcRowSet.execute();

            System.out.println("id \tfName \tlName \temail \t\tphoneNo");
            while (jdbcRowSet.next()) {
                System.out.println(jdbcRowSet.getInt("id") + "\t"
                        + jdbcRowSet.getString("firstName") + "\t"
                        + jdbcRowSet.getString("lastName") + "\t"
                        + jdbcRowSet.getString("email") + "\t"
                        + jdbcRowSet.getString("phoneNo"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
