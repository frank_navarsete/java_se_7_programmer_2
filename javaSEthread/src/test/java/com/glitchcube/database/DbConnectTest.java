package com.glitchcube.database;

import org.junit.Test;

import java.sql.*;

/**
 * @author glitchcube.com
 */
public class DbConnectTest {

    @Test
    public void connectToDB() {

        try (Connection connection = DBConnectionEstablisherLocalhost.getDbConnection()) {
            System.out.println("Database connection: Successful");
        } catch (SQLException e) {
            System.out.println("Detabase connection: Failed");
            e.printStackTrace();
        }

    }

    @Test
    public void dbQuery() {
        try(Connection connection = DBConnectionEstablisherLocalhost.getDbConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM contact");
            System.out.println("ID \tfName \tlName \temail \t\t\tphoneNo");
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + "\t"
                    + resultSet.getString("firstName") + "\t"
                    + resultSet.getString("lastName") + "\t"
                    + resultSet.getString("email") + "\t"
                    + resultSet.getString("phoneNo") + "\t");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void dbQueryWithoutInfoOnColumns() {
        try (Connection connection = DBConnectionEstablisherLocalhost.getDbConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM contact");
            int numOfColums = resultSet.getMetaData().getColumnCount();

            while (resultSet.next()) {
                for (int i = 1; i <= numOfColums; i++) {
                    System.out.print(resultSet.getObject(i) + "\t");
                }
                System.out.println("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
