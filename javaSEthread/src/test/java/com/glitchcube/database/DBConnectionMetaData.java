package com.glitchcube.database;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * @author glitchcube.com
 */
public class DBConnectionMetaData {

    @Test
    public void retrieveMetaDataFromConnection() {
        try (Connection connection = DBConnectionEstablisherLocalhost.getDbConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            System.out.println("Displaying some of the database metadata from the Connection object");
                    System.out.println("Database is: " + metaData.getDatabaseProductName() + " " +
                            metaData.getDatabaseProductVersion());
            System.out.println("Driver is: " + metaData.getDriverName() + metaData.
                    getDriverVersion());
            System.out.println("The BASE_URL for this connection is: " + metaData.getURL());
            System.out.println("User name is: " + metaData.getUserName());
            System.out.println("Maximum no. of rows you can insert is: " + metaData.
                    getMaxRowSize());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
