package com.glitchcube.stringprocessing;

import org.junit.Test;

/**
 * @author glitchcube.com
 */
public class SplittingTextTest {

    @Test
    public void splitText() {
        String string = "abcdeba";
        for (String subString : string.split("b")) {
            System.out.println(subString);
        }
        //Output without b.
    }
}
