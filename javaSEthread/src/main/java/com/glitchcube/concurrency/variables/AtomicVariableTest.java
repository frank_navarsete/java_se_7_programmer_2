package com.glitchcube.concurrency.variables;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author glitchcube.com
 */
public class AtomicVariableTest {

    private static Integer integer = new Integer(0);
    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) {
        for (int i = 0; i < 500; i++) {
            new IntegerIncrementer().start();
            new AtomicIntegerIncrementer().start();
        }
    }

    static class IntegerIncrementer extends Thread {
        @Override
        public void run() {
            System.out.println("Increment value of the integer is: " + ++integer);
        }
    }

    static class AtomicIntegerIncrementer extends Thread {
        @Override
        public void run() {
            System.out.println("Increment atomic value of the integer is: " + atomicInteger.incrementAndGet());
        }
    }
}
