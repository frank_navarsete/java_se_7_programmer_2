package com.glitchcube.concurrency.conditions;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author glitchcube.com
 */
public class RailwayStation {

    private static Lock station = new ReentrantLock();

    private static Condition joeArraivel = station.newCondition();

    public static void main(String[] args) {
        new JoeArrival().start();

        new Train("IC1234 - Paris to Munich").start();
        new Train("IC2211 - Paris to Madrid").start();
        new Train("IC1122 - Madrid to Paris").start();
        new Train("IC4321 - Munich to Paris").start();

    }

    static class Train extends Thread {

        public Train(String name) {
            setName(name);
        }

        @Override
        public void run() {
            station.lock();

            try {
                System.out.println(getName() + ": I've arrived in station");
                if (getName().startsWith("IC1122")) {
                    joeArraivel.signalAll();
                }
            } finally {
                station.unlock();
            }
        }
    }

    static class JoeArrival extends Thread {

        @Override
        public void run() {
            System.out.println("Waiting in the station for IC1122 in which Joe is coming");

            station.lock();
            try {
                joeArraivel.await();
                System.out.println("Pick up Joe and go home");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                station.unlock();
            }
        }
    }

}
